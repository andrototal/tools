import os
import argparse
import datetime


def HashType(value):
    if len(value) != 32 and len(value) != 40 and len(value) != 64:
        raise argparse.ArgumentTypeError(
            'Invalid resource hash: %s' % value)
    return value


def APIKeyType(key):
    if len(key) != 32 and len(key) != 64:
        raise argparse.ArgumentTypeError(
            'Invalid API key: len(%s) != 32|64' % key)
    return key


def APKFileType(fname):
    path = os.path.realpath(fname)

    if not os.path.isfile(path):
        raise argparse.ArgumentTypeError('File %s does not exist' % fname)
    return path


def DateType(value):
    '''
        return date time value or throw an exception
    '''
    try:
        date = datetime.datetime.strptime(value, "%Y%m%d:%H%M")
        return date
    except:
        raise argparse.ArgumentTypeError("%s is not a valid date" % value)
