'''
    contains exceptions
'''

from requests.exceptions import ConnectionError

# EXCEPTIONS HERE
class FileExistException(Exception):
    '''
        FileExistException: Exception thrown if
        the sample has already been inserted into
        the database
    '''
    def __init__(self, file):
        self.file = file
        Exception.__init__(self)

    def __str__(self):
        return repr("File %s already exists" % self.file )

class SampleNotFoundException(Exception):
    '''
        SampleNotFound: Exception thrown if
        the sample has already been inserted into
        the database
    '''
    def __init__(self, file):
        self.file = file
        Exception.__init__(self)

    def __str__(self):
        return repr("File %s already exists" % self.file )



class APKNotValid(Exception):
    '''
        APKNotValid: thrown if submitted file is not
        a valid APK file
    '''
    def __init__(self):
        self.value = "APK file is not valid"
        Exception.__init__(self)

    def __str__(self):
        return repr(self.value)

class AuthorizationException(Exception):
    '''
        AuthorizationException: thrown if authentication
        didn't complete
    '''
    def __init__(self, http_error, message):
        self.http_error = http_error
        self.message = message
        Exception.__init__(self)

class UnexpectedResponseError(Exception):
    '''
        Exception raised when the response format is not what we expected
    '''

    def __init__(self, message, response):
        Exception.__init__(self, message)
        self.response = response

class SampleAlreadyExistent(Exception):
    '''
        raised if a sample already exists in the repository
    '''
    def __init__(self, sample_id):
        self.sample_id = sample_id

    def __str__(self):
        return "The sample is already existent into the DB with id=%s" % \
            repr(self.sample_id)


class APIKeyExist(Exception):
    '''
        raised if the API Key Exists
    '''
    pass
