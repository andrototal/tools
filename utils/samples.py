'''
    Samples API helpers
'''

import requests
from requests.auth import AuthBase
from urlparse import urljoin

from exceptions import AuthorizationException
from exceptions import UnexpectedResponseError
from exceptions import SampleNotFoundException
from exceptions import ConnectionError

import json

class SamplesAuth(AuthBase):
    """Attaches Authentication to the given Request object."""

    def __init__(self, key):
        # setup any auth-related data here
        self.key = key

    def __call__(self, r):
        # modify and return the request
        r.headers['AndroTotal-Authorization'] = self.key
        return r


class SamplesEP(object):
    '''Samples entry point class, defines methods to put/get a sample'''

    def __init__(self, url, key, proxies=None):

        if proxies is None:
            self.proxies = {}
        else:
            self.proxies = proxies

        self.url = url
        self.auth = SamplesAuth(key)

        #create a session
        self.session = requests.Session()

    def set_proxies(self, proxies):
        '''
            set proxies to make requests
        '''
        self.proxies = proxies

    def get_sample(self, file_hash):
        '''
            given a sample hash try to get it
        '''

        params = {
            'file_hash' : file_hash
        }
        response = self.session.get(
                urljoin(self.url, "sample"),
                params = params,
                proxies = self.proxies,
                auth = self.auth
            )
        if response.status_code == 200:
            return response.content
        elif response.status_code == 404:
            raise SampleNotFoundException(file_hash)
        elif response.status_code == 403:
            #we cannot share this sample!
            raise AuthorizationException(403,"Sorry! We cannot share this sample")
        elif response.status_code == 401:
            raise AuthorizationException()
        elif response.status_code == 400:
            raise ValueError("Invalid hash")
        else:
            raise UnexpectedResponseError("Error", response)

    def get_sample2(self, file_hash):
        '''
            given a sample hash try to get it
        '''

        response = self.session.get(
                urljoin(self.url, "sample/%s" % file_hash),
                proxies = self.proxies,
                auth = self.auth
            )
        
        if response.status_code == 200:
            return response.content
        elif response.status_code == 404:
            raise SampleNotFoundException(file_hash)
        elif response.status_code == 403:
            #we cannot share this sample!
            raise AuthorizationException(403,"Sorry! We cannot share this sample")
        elif response.status_code == 401:
            raise AuthorizationException(401,"Authorization Required")
        elif response.status_code == 400:
            raise ValueError("Invalid hash")
        else:
            raise UnexpectedResponseError("Error", response)

    def list_samples(self, start, end):
        '''
            given a start and end date, get the list of samples that have been
            uploaded in that time span
        '''

        params = {
            'date_start' : start,
            'date_end' : end
        }

        response = self.session.get(
                urljoin(self.url, "list"),
                params = params,
                proxies = self.proxies,
                auth = self.auth
            )

        if response.status_code == 200:
            return json.loads(response.content)
        elif response.status_code == 401:
            raise AuthorizationException(401,"API Key is not valid")
        elif response.status_code == 400:
            raise ValueError("Invalid date format")
        else:
            raise UnexpectedResponseError("Error", response)

