#! /usr/bin/env python
'''
Documentation https://code.andrototal.org/tools
'''

import os
import sys
import pprint
import logging
import logging.config
from utils.types import HashType, APIKeyType, APKFileType
from uuid import UUID

logger = logging.getLogger('andrototal')


def bool_to_str(x=""):
    return {False: 'false', True: 'true'}[x]

try:
    import requests
except ImportError:
    sys.stderr.write('You must install "requests"\n')
    sys.exit(1)

try:
    import argparse
except ImportError:
    sys.stderr.write('You must either use Python 2.7 or install "argparse"\n')
    sys.exit(1)

API_ENDPOINT = 'http://api.andrototal.org/v1'


class AndroTotalClient(object):

    def __call__(self):
        self._parse()
        self._configure_logger()
        self._run()

    def _parse(self):
        def _scan(args):
            self.command = 'scan'

        def _analysis(args):
            self.command = 'analysis'

        def _result(args):
            self.command = 'result'

        parser = argparse.ArgumentParser(
            description='AndroTotal Command Line Client')

        parser.add_argument(
            '-log-level',
            '-l',
            choices=['DEBUG',
                     'INFO',
                     'WARNING',
                     'ERROR'],
            help='Logging level.',
            default='INFO')

        # sub-parsers
        subparsers = parser.add_subparsers(
            help='Available commands')

        scan = subparsers.add_parser(
            'scan',
            help='Queue a new basic scan')
        scan.set_defaults(func=_scan)

        analysis = subparsers.add_parser(
            'analysis',
            help='Get analysis by hash')
        analysis.set_defaults(func=_analysis)

        result = subparsers.add_parser(
            'result',
            help='Get the status and results of a queued scan')
        result.set_defaults(func=_result)

        for _p in (scan, analysis, result):
            _p.add_argument(
                '-at-key',
                '-a',
                type=APIKeyType,
                required=True,
                help='AndroTotal API key.')

        # scan commands
        scan.add_argument(
            '-force-scan',
            '-f',
            default=False,
            action='store_true',
            help='Whether a known APK should be scanned again')

        scan.add_argument(
            '-is-malware',
            '-M',
            default=False,
            action='store_true',
            help='Whether the submitted APK is a malware')

        scan.add_argument(
            '-no-share',
            '-S',
            default=False,
            action='store_true',
            help='Whether we can\'t share the submitted APKs')

        scan.add_argument(
            'files',
            type=APKFileType,
            nargs='+',
            help='Suspicious Android APK files. Note that flags such as '
            'tagging or is-malware will be applied to all the files.')

        # result commands
        scan.add_argument(
            '-tag',
            '-t',
            nargs='+',
            help='Tag a submitted samples. Space separated.')

        result.add_argument(
            'resource_ids',
            type=UUID,
            nargs='+',
            help='The resource ID related to a queued scan (as returned by'
            ' the scan command)')

        # analysis commands
        analysis.add_argument(
            'hashes',
            type=HashType,
            nargs='+',
            help='MD5, SHA-1 or SHA-256 of one or more APK samples')

        self.args = parser.parse_args()
        self.args.func(self.args)

        self.at_key = self.args.at_key

    def _configure_logger(self):
        stream_handler = 'logging.StreamHandler'
        try:
            import logutils
            stream_handler = 'logutils.colorize.ColorizingStreamHandler'
        except ImportError:
            pass

        logging.config.dictConfig({
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'standard': {
                    'format': '%(asctime)s [%(levelname)s] '
                    '%(name)s: %(message)s'
                },
            },
            'handlers': {
                'default': {
                    'level': getattr(logging, self.args.log_level,
                                     logging.DEBUG),
                    'class': stream_handler,
                    'formatter': 'standard'
                },
            },
            'loggers': {
                'andrototal': {
                    'handlers': ['default'],
                    'level': getattr(logging, self.args.log_level,
                                     logging.DEBUG),
                    'propagate': False
                },
            }
        })

    def _run(self):
        logger.debug('Running command: %s', self.command)
        logger.debug('AndroTotal key: %s', self.at_key)
        getattr(self, '_cmd_%s' % self.command)()

    def print_response(self, response):
        pprint.pprint(response.body)

    def _cmd_scan(self):
        for f in self.args.files:
            fn = os.path.basename(f)
            logger.debug('Uploading file %s', fn)

            variables = {
                'sample_apk': open(f),
                'is_malware': bool_to_str(self.args.is_malware),
                'force_scan': bool_to_str(self.args.force_scan),
                'share': bool_to_str((not self.args.no_share)),
                'tag': self.args.tag,
                'api_key': self.at_key
            }

            r = requests.post(
                '%s/scan/queue-basic' % (API_ENDPOINT),
                data=variables,
                files={'sample_apk': open(f)})

            print r.headers

            logger.debug('Scan response: %s', r.text)

    def _cmd_analysis(self):
        for h in self.args.hashes:
            logger.debug('Retrieving analysis %s', h)

            variables = {
                'hash': h,
                'api_key': self.at_key
            }

            r = requests.get(
                '%s/analysis' % (API_ENDPOINT),
                params=variables)

            print r.headers

            logger.debug('Analysis: %s', r.text)

    def _cmd_result(self):
        for h in self.args.resource_ids:
            logger.debug('Checking scan %s', h)

            variables = {
                'resource': h.get_hex(),
                'api_key': self.at_key
            }

            r = requests.get(
                '%s/scan/result' % (API_ENDPOINT),
                params=variables)

            logger.debug('Scan result: %s', r.text)


def main():
    return AndroTotalClient()()

if __name__ == '__main__':
    sys.exit(main())
