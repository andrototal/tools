#! /usr/bin/env python
'''
   download samples 
'''

import os
import sys
import logging
import logging.config
import datetime

try:
    import requests
except ImportError:
    sys.stderr.write('You must install "requests"\n')
    sys.exit(1)

from utils.types import HashType, DateType, APIKeyType
from utils.samples import SamplesEP
from utils.exceptions import *

from pprint import pprint

logger = logging.getLogger('andrototal')
bool_to_str = lambda x:{False: 'false', True: 'true'}[x]

try:
    import argparse
except ImportError:
    sys.stderr.write('You must either use Python 2.7 or install "argparse"\n')
    sys.exit(1)

SAMPLES_ENDPOINT = 'http://samples.andrototal.org'

class SamplesClient(object):
    def __call__(self):
        self._parse()
        self._configure_logger()
        self._run()

    def _parse(self):

        def _get(args):
            self.command = 'get'

        def _list(args):
            self.command = 'list'

        def _getbydate(args):
            self.command = 'getbydate'

        parser = argparse.ArgumentParser(
            description="Andrototal Samples API Client"
        )

        parser.add_argument(
            '-log-level',
            '-l',
            choices = [
                'DEBUG',
                'INFO',
                'WARNING',
                'ERROR'
            ],
            help = 'Logging level.',
            default = 'INFO'
        )

        subparsers = parser.add_subparsers(
            help='Available commands'
        )

        get = subparsers.add_parser('get', help='Get a sample through its hash')
        get.set_defaults(func=_get)

        get.add_argument(
            'hash',
            type = HashType,
            nargs = '+',
            help = 'Hash of the samples you want '
                   'to download from the repository'
        )

        list_samples = subparsers.add_parser('list',
            help = 'Get a list of samples submitted '
                   'to the repository in the specified time span')
        list_samples.set_defaults(func=_list)

        list_samples.add_argument(
            'start',
            type = DateType,
            help = 'Start date (format YYYmmdd:HHMM)'
        )

        list_samples.add_argument(
            'end',
            type = DateType,
            help = 'End date (format YYYYmmdd:HHMM)'
        )

        get_by_date = subparsers.add_parser('getbydate',
            help = 'Download samples submitted '
                   'to the repository in the specified time span')

        get_by_date.set_defaults(func=_getbydate)

        get_by_date.add_argument(
            'start',
            type = DateType,
            help = 'Start date (format YYYmmdd:HHMM)'
        )

        get_by_date.add_argument(
            'end',
            type = DateType,
            help = 'End date (format YYYYmmdd:HHMM)'
        )

        for _p in (get,list_samples,get_by_date):
            _p.add_argument(
                '-at-key',
                '-a',
                type=APIKeyType,
                required=True,
                help='AndroTotal API key.')

        self.args = parser.parse_args()
        self.args.func(self.args)

        self.at_key = self.args.at_key

        self.endpoint = SamplesEP(
            SAMPLES_ENDPOINT,
            self.at_key
        )

    def _configure_logger(self):
        stream_handler = 'logging.StreamHandler'
        try:
            import logutils
            stream_handler = 'logutils.colorize.ColorizingStreamHandler'
        except ImportError:
            pass

        logging.config.dictConfig({
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'standard': {
                    'format': '%(asctime)s [%(levelname)s] '\
                        '%(name)s: %(message)s'
                    },
                },
            'handlers': {
                'default': {
                    'level': getattr(logging, self.args.log_level,\
                                         logging.DEBUG),
                    'class': stream_handler,
                    'formatter': 'standard'
                    },
                },
            'loggers': {
                'andrototal': {
                    'handlers': ['default'],
                    'level': getattr(logging, self.args.log_level,\
                                         logging.DEBUG),
                    'propagate': False
                    },
                }
            })

    def _run(self):
        logger.debug('Running command: %s', self.command)
        logger.debug('Andrototal Key: %s', self.at_key)
        getattr(self,'_cmd_%s' %self.command)()

    def _cmd_get(self):
        self.get_samples(self.args.hash)


    def _cmd_list(self):
        start = self.args.start.strftime("%Y%m%d%H%M")
        end = self.args.end.strftime("%Y%m%d%H%M")
        samples = self.get_list(start,end)
        if samples is not None:
            pprint(samples)

    def _cmd_getbydate(self):
        start = self.args.start.strftime("%Y%m%d%H%M")
        end = self.args.end.strftime("%Y%m%d%H%M")
        samples_list = self.get_list(start,end)
        if samples_list is not None:
            hashes =  []
            for sample in samples_list:
                hashes.append(sample['md5'])

            self.get_samples(hashes)


    def get_list(self,start,end):
        logger.info("Searching time span: %s - %s",start,end)
        try:
            samples_list = self.endpoint.list_samples(start,end)
            return samples_list

        except UnexpectedResponseError as err:
            logger.info("Error in response: %s",err.response.status_code)
        except AuthorizationException as err:
            logger.info("Cannot complete the request, reason: %s", err.message)
        except ConnectionError as err:
            logger.error("Cannot connect to samples endpoint")
        except Exception as err:
            logger.exception("Error");

    def get_samples(self,hashes):
        for hash in hashes:
            try:
                sample = self.endpoint.get_sample2(hash)
                with open("%s.apk" % hash, "w") as f:
                    f.write(sample)
                    logger.info("Downloaded sample: %s.apk",hash)
            except SampleNotFoundException as err:
                logger.info("Sample %s not found", hash)
            except UnexpectedResponseError as err:
                logger.info("Error in response: %s",err.response.status_code)
            except AuthorizationException as err:
                logger.info("Cannot download this sample, reason: %s", err.message)
            except ConnectionError as err:
                logger.error("Cannot connect to samples endpoint")
            except Exception as err:
                logger.exception("Error");

def main():
    return SamplesClient()()

if __name__ == '__main__':
    sys.exit(main())
